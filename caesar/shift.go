package caesar

import (
	"gitlab.com/alexjohnchapman/cipherSolver/util"
	"gitlab.com/alexjohnchapman/cipherSolver/validation"
)

const naturalRank string = "etaoinshrdlucmwfygpbvkxjqz"

func ShiftString(s string, shift rune) string {
	shiftedRunes := make([]rune, len(s))
	for i, c := range s {
		shiftedRunes[i] = ShiftRune(c, shift)
	}

	return string(shiftedRunes)
}

func ShiftRune(c, shift rune) rune {
	var char rune = c
	if util.RuneIsLowerCase(c) {
		char = 97 + ((shift + c - 97) % 26)
	}
	if util.RuneIsUpperCase(c) {
		char = 65 + ((shift + c - 65) % 26)
	}
	return char
}

func Decrypt(s string) string {
	rankedShifts := rankShifts(s)
	var currentBestString string = ""
	var currentBestPercentage float32 = 100.0
	for _, shift := range rankedShifts {
		shiftedString := ShiftString(s, shift)
		numWords := util.CountWords(s)
		numInvalid := validation.NumberOfInvalidWords(shiftedString)
		percInv := percentInvalid(numWords, numInvalid)

		if percInv < 5.0 {
			return shiftedString
		}

		if percInv < currentBestPercentage {
			currentBestPercentage = percInv
			currentBestString = shiftedString
		}
	}
	return currentBestString
}

func percentInvalid(numWords, numInv int) float32 {
	return float32(numInv) / float32(numWords) * 100.0
}

func allShiftOptions(s string) []string {
	shiftedStrings := make([]string, 26)

	for i, shift := range rankShifts(s) {
		shiftedStrings[i] = ShiftString(s, shift)
	}

	return shiftedStrings
}

func orderedRunes() []rune {
	shifts := make([]rune, len(naturalRank))

	for i, letter := range naturalRank {
		shifts[i] = letter
	}

	return shifts
}

func rankShifts(s string) []rune {
	mostPopular := util.RankRunes(s)[0]
	shifts := make([]rune, 26)

	for i, r := range orderedRunes() {
		shifts[i] = (r - mostPopular) % 26
		if shifts[i] < 0 {
			shifts[i] += 26
		}
	}

	return shifts
}

func naivePlaintext(s string) string {
	return ShiftString(s, rankShifts(s)[0])
}
