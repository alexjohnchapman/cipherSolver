package caesar

import (
	"testing"
)

var testStrings []string = []string{
	"Rhythm and sound",
	"What what in the butt",
	"Hey there, I'm pleased to meet you",
	"Fuelled by cider",
	"Aardvarks are cool too",
	"Hang the painting on the wall",
	"The Prague Cemetery by Umberto Eco",
	"What do you mean?",
	"Ali asked if Andrea could fetch his Aardvark",
	"You have to go and speak to slughorn!",
	"Trust me, I know what I'm doing. Or...Felix does.",
	"Cardboard box",
	"I'm off to Hagrid's, he's a very dear friend",
	"All eyes are on you tonight",
	"Just eat proudly sponsors ITV movies",
	"Hello world",
	"Rubiks cubes are fucking dank"}

func TestPlaintext(t *testing.T) {
	for _, plainText := range testStrings {
		cipherText := ShiftString(plainText, 9)
		res := Decrypt(cipherText)

		if res != plainText {
			t.Errorf("Did not correctly decrypt ciphertext.\n Expected: %s.\n Received: %s.\n", plainText, res)
		}
	}
}
