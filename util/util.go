package util

import (
	"sort"
	"strconv"
	"strings"
)

func SplitWords(s string) []string {
	words := make([]string, CountWords(s))
	i := 0

	var startIndex, endIndex int = 0, 0
	for currentIndex, char := range s {
		if char == ' ' {
			endIndex = currentIndex
			words[i] = s[startIndex:endIndex]
			startIndex = endIndex + 1
			i++
		}
	}
	words[i] = s[startIndex:]

	return words
}

func CountWords(s string) int {
	if s == "" {
		return 0
	}

	wordCount := 1
	for _, c := range s {
		if c == ' ' {
			wordCount++
		}
	}
	return wordCount
}

func CharCount(s string) map[string]int {
	rm := runeMap(s)
	cm := make(map[string]int)
	for k, v := range rm {
		cm[strconv.QuoteRune(k)] = v
	}
	return cm
}

func RankChars(s string) []string {
	runes := RankRunes(s)

	chars := make([]string, len(runes))
	for i, c := range runes {
		chars[i] = strconv.QuoteRune(c)
	}
	return chars
}

func RankRunes(s string) []rune {
	return sortKeys(runeMap(s))
}

func RuneIsLetter(r rune) bool {
	return RuneIsLowerCase(r) || RuneIsUpperCase(r)
}

func RuneIsUpperCase(r rune) bool {
	return r >= 65 && r <= 90
}

func RuneIsLowerCase(r rune) bool {
	return r >= 97 && r <= 122
}

func runeMap(s string) map[rune]int {
	m := make(map[rune]int)
	for _, c := range strings.ToLower(s) {
		if RuneIsLowerCase(c) {
			m[c]++
		}
	}
	return m
}

func sortKeys(m map[rune]int) []rune {
	pl := make(PairList, len(m))
	i := 0

	for k, v := range m {
		pl[i] = Pair{k, v}
		i++
	}

	sort.Sort(pl)
	keys := make([]rune, len(pl))
	i--
	for _, p := range pl {
		keys[i] = p.Key
		i--
	}
	return keys
}

type Pair struct {
	Key   rune
	Value int
}

type PairList []Pair

func (p PairList) Len() int           { return len(p) }
func (p PairList) Less(i, j int) bool { return p[i].Value < p[j].Value }
func (p PairList) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }
