package main

import(
	"fmt"
	"os"
	"gitlab.com/alexjohnchapman/cipherSolver/caesar"
)

func main() {
	cipherText := os.Args[1]
	fmt.Printf("Text to decipher: %s\n", cipherText)
	plainText := caesar.Decrypt(cipherText)
	fmt.Printf("Decrypted text: %s\n", plainText)
}
