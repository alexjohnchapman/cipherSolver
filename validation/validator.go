package validation

import (
	"gitlab.com/alexjohnchapman/cipherSolver/util"
	"strings"
)

func NumberOfInvalidWords(s string) int {
	words := util.SplitWords(s)
	count := 0
	for _, word := range words {
		if !IsWordValid(word) {
			count++
		}
	}
	return count
}

func IsWordValid(word string) bool {
	lowerCaseWord := []rune(strings.ToLower(word))

	if len(lowerCaseWord) == 1 && !contains(validSingleLetterWords, lowerCaseWord[0]) {
		return false
	}

	if len(lowerCaseWord) == 2 && !strContains(allowedTwoLetterWords, strings.ToLower(word)) {
		return false
	}

	lastLetter := lowerCaseWord[len(word)-1]
	if isLastLetterInvalid(lastLetter) {
		return false
	}

	if !wordContainsSemiVowels(word) {
		return false
	}

	for i := 0; i < len(word)-1; i++ {
		if invalidPair(lowerCaseWord[i], lowerCaseWord[i+1], i) {
			return false
		}
	}

	return true
}

func invalidPair(letter, nextLetter rune, pos int) bool {
	rules := make([]bool, 12)
	rules[0] = letter == 'q' && nextLetter != 'u'
	rules[1] = letter == nextLetter && isDoubleLetterInvalid(letter)
	init := pos == 0
	rules[2] = init && contains(pickyFirstLetters, letter) && !contains(semiVowels, nextLetter)

	flRuleConstructor := func(firstLetter rune, allowedNextLetters []rune) bool {
		return init && letter == firstLetter && !contains(allowedNextLetters, nextLetter)
	}

	rules[3] = flRuleConstructor('t', []rune{'w', 'e', 'r', 'y', 'u', 'i', 'o', 'a', 'h'})
	rules[4] = flRuleConstructor('c', []rune{'a', 'e', 'r', 'y', 'u', 'i', 'o', 'h', 'l'})
	rules[5] = flRuleConstructor('g', []rune{'a', 'e', 'r', 'y', 'u', 'i', 'o', 'l'})
	rules[6] = flRuleConstructor('f', []rune{'e', 'r', 'y', 'i', 'u', 'o', 'a'})
	rules[7] = flRuleConstructor('s', []rune{'a', 'e', 'i', 'o', 'u', 'y', 'n', 'm', 'p', 'l', 'k', 'h', 'c', 't'})
	rules[8] = flRuleConstructor('x', []rune{'a', 'e', 'y'})
	rules[9] = flRuleConstructor('j', vowels)

	rules[10] = letter == 'h' && nextLetter == 'k'

	rules[11] = !contains([]rune{'e', 'a', 'o'}, letter) && nextLetter == 'x'

	for _, rule := range rules {
		if rule {
			return true
		}
	}
	return false
}

func wordContainsSemiVowels(word string) bool {
	lower := []rune(strings.ToLower(word))
	for _, letter := range lower {
		if contains(semiVowels, letter) {
			return true
		}
	}
	return false
}

func isDoubleLetterInvalid(letter rune) bool {

	return position(disallowedDoubleLetters, letter) > 0
}

func isLastLetterInvalid(letter rune) bool {

	return contains(disallowedFinalLetters, letter)
}

func contains(arr []rune, x rune) bool {
	return position(arr, x) > -1
}

func strContains(arr []string, str string) bool {
	return strPosition(arr, str) > -1
}

func strPosition(arr []string, str string) int {
	for i, s := range arr {
		if s == str {
			return i
		}
	}
	return -1
}

func position(arr []rune, x rune) int {
	for i, c := range arr {
		if x == c {
			return i
		}
	}
	return -1
}

var vowels []rune = []rune{'a', 'e', 'i', 'o', 'u'}
var semiVowels []rune = append(vowels, 'y')
var disallowedDoubleLetters []rune = []rune{'a', 'h', 'i', 'j', 'k', 'q', 'u', 'w', 'y', 'x', 'z'}
var disallowedFinalLetters []rune = []rune{'q', 'j', 'v', 'c'}
var pickyFirstLetters []rune = []rune{'h', 'l', 'm', 'j', 'v', 'z', 'x'}
var validSingleLetterWords []rune = []rune{'a', 'i'}

var allowedTwoLetterWords []string = []string{
	"as", "an", "of", "on", "it", "is", "in", "ox", "up", "we", "ow", "no", "na", "pa", "ad", "ah",
	"aw", "at", "be", "by", "da", "do", "eh", "oh", "ex", "ha", "he", "id", "la", "di", "ma", "me",
	"mr", "oi", "oy", "oz", "pi", "so", "to", "us", "uh", "um", "ye", "yo",
}
