package validation

import (
	"testing"
)

func TestDoubleLetters(t *testing.T) {
	if IsWordValid("Rakkie") {
		t.Errorf("Rakkie is an invalid word.\n")
	}

	if !IsWordValid("Address") {
		t.Errorf("Address is a real english word.\n")
	}
}

func TestLastLetter(t *testing.T) {
	if IsWordValid("Ovenv") {
		t.Errorf("Ovenv is an invalid word.\n")
	}
}

func TestPickyFirstLetters(t *testing.T) {
	if IsWordValid("Jfleu") {
		t.Errorf("Jfleu is invalid.\n")
	}
}

func TestSingleLetterWords(t *testing.T) {
	if IsWordValid("Y") {
		t.Errorf("Only \"I\" and \"A\" are valid one letter words. \"%s\" is not.", "Y")
	}
}

func TestNoVowelWords(t *testing.T) {
	if IsWordValid("Grwng") {
		t.Errorf("The word \"%s\" does not contain a vowel or a Y.", "Grwng")
	}
}

func TestNumberOfInvalidWords(t *testing.T) {
	testString := "Richard of York gave battle in vain."
	if NumberOfInvalidWords(testString) > 0 {
		t.Errorf("%s is a valid sentence.\n", testString)
	}
}
