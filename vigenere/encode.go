package vigenere

import (
	"gitlab.com/alexjohnchapman/cipherSolver/caesar"
	"gitlab.com/alexjohnchapman/cipherSolver/util"
	"strings"
)

func Encode(plainText, keyword string) string {
	lettersOnly := lettersOnly(plainText)
	shifted := make([]rune, len(lettersOnly))
	runeKey := stringKeyToRuneKey(keyword)
	j := 0
	for i, c := range lettersOnly {
		shifted[i] = caesar.ShiftRune(c, runeKey[j%len(runeKey)])
		j++
	}
	return string(shifted)
}

func lettersOnly(text string) string {
	lowerCase := strings.ToLower(text)
	lettersOnly := make([]rune, len(text))
	var j int = 0
	for _, c := range lowerCase {
		if util.RuneIsLowerCase(c) {
			lettersOnly[j] = c
			j++
		}
	}
	return string(lettersOnly[:j])
}

func stringKeyToRuneKey(key string) []rune {
	runeKey := make([]rune, len(key))
	for i, c := range strings.ToUpper(key) {
		runeKey[i] = (c - 65) % 26
	}

	return runeKey
}
