package vigenere

import (
	"testing"
)

var input string = "Hello my name is Foo. What's your name?"

func TestEncoding(t *testing.T) {
	for _, keyOutput := range keyOutputs {
		encoded := Encode(input, keyOutput.key)
		if encoded != keyOutput.output {
			t.Errorf("Did not correctly encode plaintext.\n Expected: %s\n Received: %s\n", keyOutput.output, encoded)
		}
	}
}
