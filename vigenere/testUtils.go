package vigenere

type keyOutput struct {
	key, output string
}

var keyOutputs []keyOutput = []keyOutput{
	keyOutput{"KEY", "rijvskirywigcjmyafkxqissbrywi"},
	keyOutput{"LONGERKEY", "ssyrsdiryxsvyjfyaflhfeslbryxs"},
	keyOutput{"FOOBAR", "mszmoddbonezxtcpwyfhgzolwbone"},
}
