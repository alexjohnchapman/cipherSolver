package vigenere

import (
	"testing"
)

func TestDecoding(t *testing.T) {
	for _, keyOutput := range keyOutputs {
		decoded := Decode(keyOutput.output, keyOutput.key)
		expected := "hellomynameisfoowhatsyourname"
		if decoded != expected {
			t.Errorf("Could not decode cipher text correctly.\n Expected: %s\n Received: %s\n", expected, decoded)
		}
	}
}
