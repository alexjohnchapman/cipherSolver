package vigenere

import "gitlab.com/alexjohnchapman/cipherSolver/caesar"

func Decode(cipherText, key string) string {
	runeKey := stringKeyToRuneKey(key)
	shifted := make([]rune, len(cipherText))
	j := 0
	for i, c := range cipherText {
		shifted[i] = caesar.ShiftRune(c, 26-runeKey[j%len(runeKey)])
		j++
	}
	return string(shifted)
}
